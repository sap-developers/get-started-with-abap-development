*&---------------------------------------------------------------------*
*& Report zso_invoice_items_euro
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT zso_invoice_items_euro.

CLASS lcl_main DEFINITION CREATE PRIVATE.

  PUBLIC SECTION.
    CLASS-METHODS create
      RETURNING
        VALUE(ro_instance) TYPE REF TO lcl_main.

    METHODS run.

  PROTECTED SECTION.
  PRIVATE SECTION.

ENDCLASS.

CLASS lcl_main IMPLEMENTATION.


  METHOD create.
    ro_instance = NEW lcl_main( ).
  ENDMETHOD.


  METHOD run.
    DATA(lo_invoices) = NEW zcl_invoice_retrieval( ).
    data(lt_invoice_items) = lo_invoices->get_items_from_db( ).

     cl_salv_table=>factory( IMPORTING r_salv_table = DATA(lo_salv_table)
                             CHANGING  t_table      = lt_invoice_items ).

    lo_salv_table->display( ).

  ENDMETHOD.


ENDCLASS.

START-OF-SELECTION.
  lcl_main=>create( )->run( ).
